rosservice call /kill turtle1
rosservice call /spawn 2 6 0 "turtle1"
rosservice call /turtle1/set_pen 255 0 0 2 0
rosservice call /spawn 6 6 0 "turtle2"
rosservice call /turtle2/set_pen 0 0 255 2 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, -3.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, -3.0, 0.0]' '[0.0, 0.0, 0.0]'
